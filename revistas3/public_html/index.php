

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Strategic Design Research Journal</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="generator" content="Open Journal Systems 2.3.8.0" />
	
		<link rel="stylesheet" href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/lib/pkp/styles/pkp.css" type="text/css" />
	<link rel="stylesheet" href="./lib/pkp/styles/common.css" type="text/css" />
	<link rel="stylesheet" href="./styles/common.css" type="text/css" />


	<!-- Base Jquery -->
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript">
		// Provide a local fallback if the CDN cannot be reached
		if (typeof google == 'undefined') {
			document.write(unescape("%3Cscript src='http://projeto.unisinos.br/desenvweb2/revistas3/public_html/lib/pkp/js/lib/jquery/jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
			document.write(unescape("%3Cscript src='http://projeto.unisinos.br/desenvweb2/revistas3/public_html/lib/pkp/js/lib/jquery/plugins/jqueryUi.min.js' type='text/javascript'%3E%3C/script%3E"));
		} else {
			google.load("jquery", "1.4.2");
			google.load("jqueryui", "1.8.1");
		}
	</script>
	
	
	
	<link rel="stylesheet" href="./styles/sidebar.css" type="text/css" />		<link rel="stylesheet" href="./styles/rightSidebar.css" type="text/css" />	
			<link rel="stylesheet" href="./public/journals/20/journalStyleSheet.css" type="text/css" />
	
	<script type="text/javascript" src="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/lib/pkp/js/general.js"></script>
	<script type="text/javascript" src="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/lib/pkp/js/tag-it.js"></script>
	<!-- Add javascript required for font sizer -->
	<script type="text/javascript" src="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/lib/pkp/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/lib/pkp/js/fontController.js" ></script>
	<script type="text/javascript">
		$(function(){
			fontSize("#sizer", "body", 9, 16, 32, "/desenvweb2/revistas3/public_html"); // Initialize the font sizer
		});
	</script>

	<script type="text/javascript">
        // initialise plugins
		
        $(function(){
        	
		});
		
	</script>

	
	<link rel="alternate" type="application/atom+xml" href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/gateway/plugin/WebFeedGatewayPlugin/atom" />
	<link rel="alternate" type="application/rdf+xml" href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/gateway/plugin/WebFeedGatewayPlugin/rss" />
	<link rel="alternate" type="application/rss+xml" href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/gateway/plugin/WebFeedGatewayPlugin/rss2" />
</head>
<body>
<div id="container">

<div id="header">
<div id="headerTitle">
<h1>
	Strategic Design Research Journal
</h1>
</div>
</div>

<div id="body">

	<div id="sidebar">
							<div id="rightSidebar">
				<div class="block" id="sidebarUser">
			<span class="blockTitle">User</span>
		
						<form method="post" action="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/login/signIn">
				<table>
					<tr>
						<td><label for="sidebar-username">Username</label></td>
						<td><input type="text" id="sidebar-username" name="username" value="" size="20" maxlength="32" class="textField" /></td>
					</tr>
					<tr>
						<td><label for="sidebar-password">Password</label></td>
						<td><input type="password" id="sidebar-password" name="password" value="" size="20" maxlength="32" class="textField" /></td>
					</tr>
					<tr>
						<td colspan="2"><input type="checkbox" id="remember" name="remember" value="1" /> <label for="remember">Remember me</label></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Log In" class="button" /></td>
					</tr>
				</table>
			</form>
			</div><div class="block" id="sidebarNavigation">
	<span class="blockTitle">Journal Content</span>
	
	<span class="blockSubtitle">Search</span>
	<form method="post" action="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/search/results">
	<table>
	<tr>
		<td><input type="text" id="query" name="query" size="32" maxlength="255" value="" class="textField" /></td>
	</tr>
	<tr>
		<td><select name="searchField" size="1" class="selectMenu">
			<option label="All" value="">All</option>
<option label="Authors" value="1">Authors</option>
<option label="Title" value="2">Title</option>
<option label="Abstract" value="4">Abstract</option>
<option label="Index terms" value="120">Index terms</option>
<option label="Full Text" value="128">Full Text</option>

		</select></td>
	</tr>
	<tr>
		<td><input type="submit" value="Search" class="button" /></td>
	</tr>
	</table>
	</form>
	
	<br />
	
		<span class="blockTitle">Browse</span>
	<ul class="none">
		<li><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/issue/archive">By Issue</a></li>
		<li><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/search/authors">By Author</a></li>
		<li><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/search/titles">By Title</a></li>
				<li><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/index">Other Journals</a></li>
			</ul>
	</div>
<div class="block" id="sidebarLanguageToggle">
	<span class="blockTitle">Language</span>
	<form action="#">
		<select size="1" name="locale" onchange="location.href=('http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/user/setLocale/NEW_LOCALE?source=%2Fdesenvweb2%2Frevistas3%2Fpublic_html%2Findex.php%2Fsdrj'.replace('NEW_LOCALE', this.options[this.selectedIndex].value))" class="selectMenu"><option label="English" value="en_US" selected="selected">English</option>
<option label="Português (Brasil)" value="pt_BR">Português (Brasil)</option>
</select>
	</form>
</div>
<div class="block" id="sidebarFontSize" style="margin-bottom: 4px;">
	<span class="blockTitle">Font Size</span>
	<div id="sizer"></div>
</div>
<br /><div class="block" id="sidebarDevelopedBy">
	<a class="blockTitle" href="http://pkp.sfu.ca/ojs/" id="developedBy">Open Journal Systems</a>
</div>	<div class="block" id="sidebarHelp">
	<a class="blockTitle" href="javascript:openHelp('http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/help/view/user/topic/000001')">Journal Help</a>
</div> <div class="block" id="notification">
	<span class="blockTitle">Notifications</span>
	<ul class="none">
					<li><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/notification">View</a></li>
			<li><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/notification/subscribeMailList">Subscribe</a> / <a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/notification/unsubscribeMailList">Unsubscribe</a></li>	
			</ul>
</div>

			</div>
			</div>

<div id="main">
<div id="navbar">
	<ul class="menu">
		<li id="home"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/index">Home</a></li>
		<li id="about"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/about">About</a></li>

					<li id="login"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/login">Log In</a></li>
							<li id="register"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/user/register">Register</a></li>
										<li id="search"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/search">Search</a></li>
		
					<li id="current"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/issue/current">Current</a></li>
			<li id="archives"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/issue/archive">Archives</a></li>
		
					<li id="announcements"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/announcement">Announcements</a></li>
				

									<li id="navItem"><a href="http://revistas.unisinos.br/index.php/sdrj/about/submissions#onlineSubmissions">Submission</a></li>
												<li id="navItem"><a href="http://revistas.unisinos.br/index.php/sdrj/pages/view/etica">Ethics Policy</a></li>
												<li id="navItem"><a href="http://revistas.unisinos.br/index.php/sdrj/pages/view/indexada">Indexing</a></li>
																	<li id="navItem"><a href="http://revistas.unisinos.br/index.php/sdrj/about/contact">Contact</a></li>
											</ul>
</div>

<div id="breadcrumb">
	<a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/index">Home</a> &gt;
			<a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj" class="current">Vol 6, No 3 (2013)</a></div>

<h2>Strategic Design Research Journal</h2>


<div id="content">








		<div id="announcementsHome">
		<h3>Announcements</h3>
		<table class="announcements">
	<tr>
		<td colspan="2" class="headseparator">&nbsp;</td>
	</tr>
	<tr class="title">
			<td class="title"><h4>Call for papers: “Moda como expressão de cultura”</h4></td>
			<td class="more">&nbsp;</td>
	</tr>
	<tr class="description">
		<td class="description"><p><strong>Editores:</strong> Ione Bentz (Universidade do Vale do Rio dos Sinos), Fábio Parode (Universidade do Vale do Rio dos Sinos) e Bruna Ruschel (Universidade do Vale do Rio dos Sinos).</p><p><strong>Volume 8, número 3 (set/dez, 2015) </strong></p><p><strong>Submissões: até 01 de setembro de 2015</strong></p></td>
		<td class="more">&nbsp;</td>
	</tr>
	<tr class="details">
		<td class="posted">Posted: 2014-10-17</td>
					<td class="more"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/announcement/view/20">More...</a></td>
			</tr>
	<tr>
		<td colspan="2" class="endseparator">&nbsp;</td>
	</tr>
</table>
	
		<table class="announcementsMore">
			<tr>
				<td><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/announcement">More Announcements...</a></td>
			</tr>
		</table>
	</div>

		<br />
	<h3>Vol 6, No 3 (2013): Sep/Dec</h3>
		<div id="issueDescription"></div>
	<h3>Table of Contents</h3>
	 

		
<table class="tocArticle" width="100%">
<tr valign="top">
		

				
					
	<td class="tocTitle">Presentation</td>
	<td class="tocGalleys">
									<a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/8086/4237" class="file">PDF (Português (Brasil))</a>
														
			</td>
</tr>
<tr>
	<td class="tocAuthors">
									Fabio Parode						</td>
	<td class="tocPages">95</td>
</tr>
</table>

<div class="separator"></div>
<h4 class="tocSectionTitle">Articles</h4>
		
<table class="tocArticle" width="100%">
<tr valign="top">
		

				
					
	<td class="tocTitle"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.01">Approaches to the development and design of service systems: A comparative analysis of design methods and its relation to the scope of services</a></td>
	<td class="tocGalleys">
									<a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.01/4238" class="file">PDF (Português (Brasil))</a>
														
			</td>
</tr>
<tr>
	<td class="tocAuthors">
									Lethícia Mallet,							Maria Clara Lippi						</td>
	<td class="tocPages">96-104</td>
</tr>
</table>
		
<table class="tocArticle" width="100%">
<tr valign="top">
		

				
					
	<td class="tocTitle"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.02">Using system analysis to deepen the understanding of open and user-driven innovation initiatives</a></td>
	<td class="tocGalleys">
									<a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.02/4239" class="file">PDF</a>
														
			</td>
</tr>
<tr>
	<td class="tocAuthors">
									Miriam de Magdala Pinto,							Letícia Pedruzzi Fonseca						</td>
	<td class="tocPages">105-115</td>
</tr>
</table>
		
<table class="tocArticle" width="100%">
<tr valign="top">
		

				
					
	<td class="tocTitle"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.03">The implications of the choice overload effect on users to the design of Product-Service Systems</a></td>
	<td class="tocGalleys">
									<a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.03/4240" class="file">PDF (Português (Brasil))</a>
														
			</td>
</tr>
<tr>
	<td class="tocAuthors">
									Felipe Gerenda,							Leandro Miletto Tonetto						</td>
	<td class="tocPages">116-126</td>
</tr>
</table>
		
<table class="tocArticle" width="100%">
<tr valign="top">
		

				
					
	<td class="tocTitle"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.04">Design management in mariculture as a factor for strategic competitiveness</a></td>
	<td class="tocGalleys">
									<a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.04/4241" class="file">PDF (Português (Brasil))</a>
														
			</td>
</tr>
<tr>
	<td class="tocAuthors">
									Michela Cristiane França Goulart,							Eugenio Andrés Díaz Merino,							Giselle Schmidt Alves Díaz Merino						</td>
	<td class="tocPages">127-136</td>
</tr>
</table>
		
<table class="tocArticle" width="100%">
<tr valign="top">
		

				
					
	<td class="tocTitle"><a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.05">Discourse analysis of the good form concept in Industrial Design in Switzerland and in Germany between 1948 and 1968</a></td>
	<td class="tocGalleys">
									<a href="http://projeto.unisinos.br/desenvweb2/revistas3/public_html/index.php/sdrj/article/view/sdrj.2013.63.05/4242" class="file">PDF (Português (Brasil))</a>
														
			</td>
</tr>
<tr>
	<td class="tocAuthors">
									Rodrigo da Silva Paiva						</td>
	<td class="tocPages">137-146</td>
</tr>
</table>



<br /><br />
<table border="0" width="598"><tbody><tr><td colspan="3" align="left" valign="top">ISSN 1984-2988 - <strong>Best viewed in Mozilla Firefox</strong><p><a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img style="border-width: 0px;" src="http://i.creativecommons.org/l/by/3.0/80x15.png" alt="Licença Creative Commons" /> </a> <br /><span>This work is licensed under a</span> <a rel="license" href="http://creativecommons.org/licenses/by/3.0/" target="_new">Licença Creative Commons Attribution 3.0</a></p></td></tr> <tr><td colspan="3" align="left" valign="top"><strong>São Leopoldo, RS</strong>. Av. Unisinos, 950. Bairro Cristo Rei, CEP: 93.022-000. Atendimento Unisinos +55 (51) 3591 1122</td></tr> <tr><td colspan="3" align="left" valign="top"></td></tr><tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td width="149" align="left" valign="top"><a href="/index.php/sdrj/pages/view/crosscheck" target="_blank"><img src="/plugins/themes/images/crosscheck_depositor.png" alt="" /></a></td> <td width="1" align="left" valign="top"></td> <td width="149" align="right" valign="top"><a href="http://www.crossref.org/05researchers/index.html" target="_blank"><img src="/plugins/themes/images/crossref_member.gif" alt="" /></a></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"></td></tr> <tr><td colspan="3" align="center" valign="top"><br /></td></tr></tbody></table>
<!-- Google Analytics -->
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-3433223-60";
urchinTracker();
</script>
<!-- /Google Analytics -->

</div><!-- content -->
</div><!-- main -->
</div><!-- body -->



</div><!-- container -->
</body>
</html>

